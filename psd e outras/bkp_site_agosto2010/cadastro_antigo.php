<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><!-- InstanceBegin template="/Templates/menu.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" --> 
<title>D&amp;S Design &amp; Solu&ccedil;&otilde;es /// Suportes para Caixa Ac&uacute;stica, 
LCD, Plasma e Projetor</title>
<!-- InstanceEndEditable --> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
//-->
</script>
<style type="text/css">
.combomeu{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	line-height: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	color: #000000;
	width: 150px;
}
.menumeu{
	width: 758px;
	background-position: center;
}
</style>
<!-- InstanceBeginEditable name="head" --> 
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' inválido.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' necessário.\n'; }
  } if (errors) alert('Ocorreu os seguinte erro\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
<style type="text/css">
<!--
@import url("estilos/estilos.css");
-->
</style>
<!-- InstanceEndEditable -->
<link href="../../ds.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#DFDFDF" background="../../imagens/pixels.jpg" link="#333333" vlink="#333333" alink="#333333" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('../../imagens/empresa_menu_topo_on.jpg','../../imagens/atendimento_menu_topo_on.jpg','../../imagens/onde_menu_topo_on.jpg','../../imagens/cadastro_menu_topo_on.jpg')">
<table width="758" height="100" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr valign="top"> 
    <td height="100" bgcolor="#C1C1C1"> 
      <div align="left"><img src="../../imagens/topo_produtos.jpg" width="600" height="107" border="0" usemap="#Map"></div></td>
    <td width="158">&nbsp;</td>
  </tr>
</table>
<table width="758" height="41" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="18" bgcolor="#C1C1C1">&nbsp;</td>
    <td width="125" valign="middle" bgcolor="#C1C1C1"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong><font size="4">SUPORTES</font> 
      </strong></font></td>
    <td width="210" valign="middle" bgcolor="#C1C1C1"><div align="right"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong>Encontre 
        o Suporte adequado<br>
        para o seu equipamento:</strong></font></div></td>
    <td width="6" valign="middle" bgcolor="#C1C1C1"> <div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"> 
        </font></div></td>
    <td width="241" valign="middle" bgcolor="#C1C1C1"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"> 
      <select name="menu1" size="1" onChange="MM_jumpMenu('parent',this,0)">
        <option selected>Modelos D&amp;S</option>
        <option>------------------------------------</option>
        <option>TV/Monitor LCD - Fixos</option>
        <option value="arezzo.htm">Arezzo 23&quot; a 37&quot; - 40&quot; a 50&quot;</option>
        <option value="millano.htm">Millano 23&quot; a 37&quot; - 37&quot; a 50&quot;</option>
        <option value="rimini.htm">Rimini 15&quot; a 23&quot; - 23&quot; a 32&quot;</option>
        <option value="verona.htm">Verona 15&quot; a 23&quot;</option>
        <option value="maranello.htm">Maranello 15&quot; a 23&quot;</option>
        <option>------------------------------------</option>
        <option>TV/Monitor LCD - M&oacute;veis</option>
        <option value="veneza.htm">Veneza 15&quot; a 23&quot;</option>
        <option value="arezzo_flex.htm">Arezzo Flex 23&quot; a 37&quot;</option>
        <option>------------------------------------</option>
        <option>TV Plasma/LCD/LED</option>
        <option value="arezzo.htm">Arezzo 23&quot; a 37&quot; - 40&quot; a 50&quot;</option>
        <option value="turim.htm">Turim 40&quot; a 50&quot;</option>
        <option value="turim_flex.htm">Turim Flex 40&quot; a 50&quot;</option>
        <option value="roma.htm">Roma 20&quot; a 60&quot;</option>
        <option>------------------------------------</option>
        <option>Projetor</option>
        <option value="trento.htm">Trento</option>
        <option>------------------------------------</option>
        <option>Caixas Ac&uacute;sticas Peq.</option>
        <option value="genova.htm">Genova - M&oacute;vel - at&eacute; 1KG</option>
        <option value="bergamo.htm">Bergamo - M&oacute;vel - at&eacute; 1KG</option>
        <option value="bari.htm">Bari - M&oacute;vel - at&eacute; 3KG</option>
        <option value="padua.htm">Padua - M&oacute;vel - at&eacute; 3KG</option>
        <option>------------------------------------</option>
        <option>Caixa Ac&uacute;stica M&eacute;dia </option>
        <option value="rovigo.htm">Rovigo - M&oacute;vel - At&eacute; 6kg</option>
        <option>------------------------------------</option>
      </select>
      </font></td>
    <td width="158" valign="middle">&nbsp;</td>
  </tr>
</table>
<table width="758" height="42" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="21" height="40" bgcolor="#FFFFFF">&nbsp;</td>
    <td width="579" bgcolor="#FFFFFF"><font color="#FF0000" size="4" face="Verdana, Arial, Helvetica, sans-serif"><!-- InstanceBeginEditable name="conteudo3" -->Cadastro<!-- InstanceEndEditable --></font></td>
    <td width="158" rowspan="2">&nbsp;</td>
  </tr>
  <tr valign="top"> 
    <td height="2" colspan="2"><img src="../../linhas.jpg" width="600" height="2"></td>
  </tr>
</table>
<table width="758" height="310" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="22" height="184" bgcolor="#FFFFFF"> <div align="left"></div></td>
    <td width="578" valign="top" bgcolor="#FFFFFF"> 
      <div align="left"><!-- InstanceBeginEditable name="conteudo02" --> 
        <form action="formulario.php" method="post" name="form1" onSubmit="MM_validateForm('empresa','','R','contato','','R','cnpj','','R','ie','','R','endereco','','R','cidade','','R','estado','','R','cep','','R','fone','','R','email','','RisEmail','ramo','','R');return document.MM_returnValue">
          <div align="left">
            <div align="left"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong> 
              <img src="imagens/borda.gif" width="1" height="8"><br>
              <?
		  if ($var=="true"){
		  echo '<table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" class="bordacinza">
  <tr bgcolor="#F9F9F9">
    <td><img src="imagens/icone_contato.gif" hspace="5"></td>
    <td width="95%" bgcolor="#F9F9F9" class="textopreto"><b>E-Mail enviado com sucesso!</b><br>
      Seus dados foram encaminhados para a &aacute;rea respons&aacute;vel e ser&atilde;o respondidos o mais r&aacute;pido poss&iacute;vel.</td>
  </tr>
</table>';
										}
		  ?>
              Preencha as informa&ccedil;&otilde;es abaixo. </strong><font color="#FF0000">*Campos 
              obrigat&oacute;rios.</font></font> <br>
              <br>
              <font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif">Empresa<font color="#FF0000">*</font><img src="imagens/borda.gif" width="287" height="1">Contato<font color="#FF0000">*</font><br>
              <input name="empresa" type="text" id="empresa" size="52">
              <input name="contato" type="text" id="contato">
              <br>
              <br>
              CNPJ<font color="#FF0000">*</font><img src="imagens/borda.gif" width="116" height="1">Inscr. 
              Estadual<font color="#FF0000">*</font><br>
              <input name="cnpj" type="text" id="cnpj">
              <input name="ie" type="text" id="ie">
              <br>
              <br>
              Endere&ccedil;o<font color="#FF0000">*</font><img src="imagens/borda.gif" width="243" height="1">Cidade<font color="#FF0000">*</font><img src="imagens/borda.gif" width="104" height="1">Estado<font color="#FF0000">*</font><br>
              <input name="endereco" type="text" id="endereco" size="45">
              <input name="cidade" type="text" id="cidade">
              <input name="estado" type="text" id="estado" size="2">
              <br>
              <br>
              Cep<font color="#FF0000">*</font><img src="imagens/borda.gif" width="121" height="1">Fone<font color="#FF0000">*</font><img src="imagens/borda.gif" width="116" height="1">Fax<br>
              <input name="cep" type="text" id="cep">
              <input name="fone" type="text" id="fone">
              <input name="fax" type="text" id="fax">
              <br>
              <br>
              E-mail<img src="imagens/borda.gif" width="115" height="1">Site<br>
              <input name="email" type="text" id="email">
              <input name="site" type="text" id="site">
              <br>
              <br>
              Ramo de Trabalho<font color="#FF0000">*</font><br>
              <input name="ramo" type="text" id="ramo">
              <br>
              <br>
              <input type="radio" name="local" value="instalador">
              Instalador<font color="#FF0000">*</font><img src="imagens/borda.gif" width="10" height="1"> 
              <input type="radio" name="local" value="loja">
              Loja<font color="#FF0000">*</font><br>
              <br>
              <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','imagens/enviar_on.jpg',1)"> 
              </a><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><font color="#666666"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><font color="#666666"><strong>
              <input name="image" type="image" value="submit" src="imagens/enviar_off.jpg" alt="Enviar" width="50" height="15" border="0">
              </strong></font></font></font></font></font><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image18','','imagens/enviar_on.jpg',1)"> 
              </a></font><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><font color="#666666" size="1"><strong> 
              </strong></font></font></font> </div>
          </div>
        </form>
        <!-- InstanceEndEditable --></div>
      </td>
    <td width="158"> 
      <div align="left"></div></td>
  </tr>
</table>
<table width="758" height="2" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td height="2" valign="top"><img src="../../linhas_divisao.jpg" width="600" height="2"></td>
  </tr>
</table>
<table width="758" height="36" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr valign="middle"> 
    <td width="10" height="36" bgcolor="#FF0000"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><strong></strong></font></td>
    <td width="14" bgcolor="#FFFFFF">&nbsp;</td>
    <td width="175" bgcolor="#FFFFFF"><font color="#333333" size="1" face="Verdana, Arial, Helvetica, sans-serif"><img src="../../imagens/produto_brasileiro.jpg" width="114" height="15"></font></td>
    <td bgcolor="#FFFFFF"> <div align="center"><a href="../../empresa.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image54','','../../imagens/empresa_menu_topo_on.jpg',1)"><img src="../../imagens/empresa_menu_topo_off.jpg" name="Image54" width="56" height="18" border="0"></a><img src="../../imagens/barra.jpg" width="6" height="18"> 
        <a href="../../atendimento.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image56','','../../imagens/atendimento_menu_topo_on.jpg',1)"><img src="../../imagens/atendimento_menu_topo_off.jpg" name="Image56" width="136" height="18" border="0"></a> 
        <img src="../../imagens/barra.jpg" width="6" height="18"> <a href="../../onde_comprar.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image58','','../../imagens/onde_menu_topo_on.jpg',1)"><img src="../../imagens/onde_menu_topo_off.jpg" name="Image58" width="83" height="18" border="0"></a> 
        <a href="../../atendimento.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image56','','../../imagens/atendimento_menu_topo_on.jpg',1)"></a> 
        <img src="../../imagens/barra.jpg" width="6" height="18"> <a href="../../onde_comprar.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image58','','../../imagens/onde_menu_topo_on.jpg',1)"></a><a href="../../cadastro.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image11','','../../imagens/cadastro_menu_topo_on.jpg',1)"><img src="../../imagens/cadastro_menu_topo_off.jpg" name="Image11" width="56" height="18" border="0"></a> 
      </div></td>
    <td width="158">&nbsp;</td>
  </tr>
</table>

<map name="Map"><area shape="rect" coords="470,7,590,99" href="../../index_site.htm">
</map></body>
<!-- InstanceEnd --></html>
