<title>D&amp;S Design &amp; Solu&ccedil;&otilde;es /// Suportes para Caixa Ac&uacute;stica, LCD, Plasma e Projetor</title><body text="000000" link="000000" vlink="000000" alink="000000">
<p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#000000"><b><font face="Arial, Helvetica, sans-serif">E-mail 
  enviado com sucesso. </font></b></font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2" color="#000000">Esta 
  p&aacute;gina ser&aacute; redirecionada para a anterior em 3 segundos.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="1" color="#000000">(caso 
  isso n&atilde;o ocorra, <a href="javascript:history.back(1)">clique aqui</a>).</font></p>
<?php

//Email da empresa
$destinatario = "atendimento@dssuportes.com.br";
//Recupera o email de quem est� enviando
$origem = $email;
//Campo "assunto" do email"
$assunto = "Formul�rio de Cadastro - DS SUPORTES";
//Aqui vai a mensagem  em html(entre as aspas simples)
$mensagem  = '<table width="90%" border="0" cellspacing="5" cellpadding="5">
  <tr> 
    <td bgcolor="F9F9F9" height="30"><font color="#999999" size="2" face="Verdana, Arial, Helvetica, sans-serif"><b><font color="#000000" face="Arial, Helvetica, sans-serif">Formul&aacute;rio 
      <u>Cadastro</u> preenchido no site - DS SUPORTES:</font></b></font></td>
  </tr>
  <tr> 
    <td bgcolor="#FFFFFF"> <hr color="#000000" width="100%" size="1" noshade> 
      <table width="100%" cellpadding="5" cellspacing="3">
        <tr valign="top" bgcolor="F9F9F9"> 
          <td width="35%"><font size="2" face="Arial, Helvetica, sans-serif" color="#333333"><b>Empresa:</b></font></td>
          <td width="65%"><font color="#666666" face="Arial, Helvetica, sans-serif"><font size="2"> 
            '.$empresa.'</font></font></td>
        </tr>
        <tr valign="top" bgcolor="ffffff"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Contato: 
            </font></b></font></td>
          <td><p><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$contato.'</font></p></td>
        </tr>
        <tr valign="top" bgcolor="F9F9F9"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">CNPJ: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$cnpj.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="ffffff"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Inscr. 
            Estadual: </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$ie.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="F9F9F9"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Endere&ccedil;o: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$endereco.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="ffffff"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Cidade: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$cidade.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="F9F9F9"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Estado: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$estado.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="ffffff"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">CEP: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$cep.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="F9F9F9"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Fone</font><font color="#333333" size="2">: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$fone.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="ffffff"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Fax</font><font color="#333333" size="2">: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$fax.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="F9F9F9"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">E-Mail: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$email.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="ffffff"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Site</font><font color="#333333" size="2">: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$site.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="F9F9F9"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Ramo 
            de Trabalho: </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$ramo.' 
            </font></td>
        </tr>
        <tr valign="top" bgcolor="ffffff"> 
          <td><font face="Arial, Helvetica, sans-serif"><b><font color="#333333" size="2">Local: 
            </font></b></font></td>
          <td><font size="2" face="Arial, Helvetica, sans-serif" color="#666666">'.$local.' 
            </font></td>
        </tr>
      </table>
      <hr color="#000000" width="100%" size="1" noshade> </td>
  </tr>
</table>';
 

//N�o alterar!!!!!
    $headers ="Content-Type: text/html; charset=iso-8859-1\n"; 
    $headers.="From: $origem\n"; 
    mail("$destinatario", "$assunto", "$mensagem", "$headers"); 


echo '<META HTTP-EQUIV="Refresh" CONTENT="0;URL=cadastro.php?var=true">';

?>